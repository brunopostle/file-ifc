package File::IFC;

use strict;
use warnings;
use 5.010;
use lib ('lib', '../lib');
use File::IFC::Header;
use File::IFC::Entity;
use Digest::MD5;
use Exporter;
use vars qw /@EXPORT_OK/;

use base 'Exporter';
@EXPORT_OK = qw /guid real/;

=head1 NAME

File::IFC - Industry Foundation Classes

=head1 SYNOPSIS

A tool for writing IFC STEP files for Building Information Modelling (BIM)

=head1 DESCRIPTION

File::IFC contains basic methods for reading and writing IFC files. To write real-world
IFC files you probably want to use the sub-class L<File::IFC::Helpers>, which contains
methods for assembling headers, components etc...

=cut

our $VERSION = 0.01;

use vars qw /$AUTOLOAD/;

=pod

=head1 METHODS

=over

=item new

Create a File::IFC project like so:

  use File::IFC;
  $ifc = File::IFC->new;

=cut

sub new
{
    my $class = shift;

    my $self = {header => {}, data => [], products => {}, presentations => {}, _cache_id => {}};

    bless $self, $class;
    return $self;
}

=item FILE_XXXX

Add a header item to the project, these methods are created on the fly:

  $ifc->FILE_FOP ('Some text', ['a', 'sub', 'list']);

=item IFCXXXXX

Add entities to the project, these methods are created on the fly:

  $foo = $ifc->IFCFOO;
  $foo->Add ('Some text', undef, 12.0);
  $bar = $ifc->IFCBAR ('Some other text', undef, 12.03, '.ELEMENT.', 1.0E-05);
  $ifc->IFCBAZ (undef, 'More text', $foo, $bar);
  $ifc->IFCNOO ($foo, [0.0,1.0,0.0], undef, [$foo, $bar]);

Note, to force a number to be interpreted as a string, prefix it with 'STRING:'

  $ifc->IFCNAR ('Year', 'STRING:2013');

Note, this library does _no_ entity validation, you can create any method name you like,
say $ifc->IFCWHATEVER(undef), and #123=IFCWHATEVER($); will be written to the file.

=cut

sub AUTOLOAD
{
    my ($self, @param) = @_;
    my $class = $AUTOLOAD;
    $class =~ s/.*:://x;
    if ($class =~ /^IFC/xi)
    {
        my $entity = File::IFC::Entity->new ($class, $self);
        $entity->Add (@param);
        push @{$self->{data}}, $entity;
        return $entity;
    }
    else
    {
        my $header = File::IFC::Header->new ($class, $self);
        $header->Add (@param);
        $self->{header}->{$class} = $header;
        return $header;
    }
}

# IFCCARTESIANPOINT AND IFCDIRECTION are just lists of floats, force a float representation

sub IFCCARTESIANPOINT
{
    my ($self, $coor) = @_;
    my $entity = File::IFC::Entity->new ('IFCCARTESIANPOINT', $self);
    $entity->Add ([map {real($_)} @{$coor}]) if defined $coor;
    push @{$self->{data}}, $entity;
    return $entity;
}

sub IFCDIRECTION
{
    my ($self, $coor) = @_;
    my $entity = File::IFC::Entity->new ('IFCDIRECTION', $self);
    $entity->Add ([map {real($_)} @{$coor}]) if defined $coor;
    push @{$self->{data}}, $entity;
    return $entity;
}

sub DESTROY
{
    my $self = shift;
    return;
}

=item Parse

Alternatively, parse existing IFC data (this expects an array of lines):

  $ifc->Parse (@lines);

=cut

sub Parse
{
    my ($self, @orig) = @_;
    my @lines;
    my $state = 'NEXT';
    for my $line (@orig)
    {
        $line =~ s/(\r|\n|\l)//xg;
        push @lines, $line if $state eq 'NEXT';
        $lines[-1] .= $line if $state eq 'CONTINUE';
        if ($line =~ /;[[:space:]]*$/x)
        {
            $state = 'NEXT';
        }
        else
        {
            $state = 'CONTINUE';
        }
    }
    $state = 'START';
    for my $line (@lines)
    {
        if ($line =~ /^HEADER;/x)
        {
            $state = 'HEADER';
            next;
        }
        if ($line =~ /^DATA;/x)
        {
            $state = 'DATA';
            next;
        }
        if ($line =~ /^ENDSEC;/x)
        {
            $state = 'ENDSEC';
        }
        if ($state eq 'HEADER')
        {
            my $header = File::IFC::Header->new (undef, $self);
            $header->Parse ($line);
        }
        if ($state eq 'DATA')
        {
            my $entity = File::IFC::Entity->new (undef, $self);
            $entity->Parse ($line);
        }
        if ($state eq 'ENDSEC')
        {
            return 1 if $line =~ /^END-ISO/x;
        }
    }
    return 0;
}

=item Dump Text

Generate a IFC formatted data as an array of lines:

  @list = $ifc->Dump;

..or join it all together as a single text blob:

  print $ifc->Text;

This results in text output like so:

  ISO-10303-21;
  HEADER;
  FILE_FOP('Some text',('a','sub','list'));
  ENDSEC;
  DATA;
  #1=IFCFOO('Some text',$,12);
  #2=IFCBAR('Some other text',$,12.03,.ELEMENT.,1.E-05);
  #3=IFCBAZ($,'More text',#1,#2);
  #4=IFCNOO(#1,(0,1,0),$,(#1,#2));
  #5=IFCNAR('Year','2013');
  ENDSEC;
  END-ISO-10303-21;

=cut

sub Dump
{
    my $self = shift;
    $self->Cache_Ids;
    my @list;
    push @list, "ISO-10303-21;";
    push @list, "HEADER;";
    for my $key (sort keys %{$self->{header}})
    {
        push @list, $self->{header}->{$key}->Dump;
    }
    push @list, "ENDSEC;";
    push @list, "DATA;";
    my $id = 1;
    for my $entity (@{$self->{data}})
    {
        $id++;
        next unless defined $entity;
        push @list, $entity->Dump;
    }
    push @list, "ENDSEC;";
    push @list, "END-ISO-10303-21;";

    return @list;
}

sub Text
{
    my $self = shift;
    return join "\n", $self->Dump, '';
}

=item Cache_Ids

Build a lookup table of entity ids (optional, this just speeds up the Get_Id() method):

  $ifc->Cache_Ids;

=cut

sub Cache_Ids
{
    my $self = shift;
    $self->{_cache_id} = {};
    for my $id (1 .. scalar @{$self->{data}})
    {
        next unless defined $self->by_id($id);
        $self->{_cache_id}->{$self->by_id($id)} = "#$id";
    }
    return;
}

=item Renumber Merge_Ifcrel

Go through list of entities and renumber without changing order (the data is stored as a big list, this just deletes empty list entries):

  $ifc->Renumber;

Merge entities that specify a relationship such as IFCRELASSOCIATESMATERIAL, IFCRELCONTAINEDINSPATIALSTRUCTURE and IFCRELAGGREGATES:

  $ifc->Merge_Ifcrel;

i.e. multiple entries like this get removed:

  #25=IFCRELAGGREGATES('0wsQ2vBzIYI3uTHLpG2INA',#5,$,$,#19,(#24));
  #30=IFCRELAGGREGATES('0dUwp9Ptxt608YBTVMwlQQ',#5,$,$,#19,(#29));

..and replaced by a single entry like this (note that as entries are removed, subsequent entries are renumbered):

  #1439=IFCRELAGGREGATES('2OknSXOQLN6pcUngENnLLw',#5,$,$,#19,(#24,#28));

=cut

sub Renumber
{
    my $self = shift;
    my $id = 0;
    while ($id < scalar @{$self->{data}})
    {
        if (!defined $self->{data}->[$id])
        {
            splice @{$self->{data}}, $id, 1;
        }
        else
        {
            $id++;
        }
    }
    $self->{_cache_id} = {};
    return;
}

sub Merge_Ifcrel
{
    my $self = shift;
    my $ifcrelassociatesmaterial = {};
    my $ifcrelcontainedinspatialstructure = {};
    my $ifcreldefinesbytype = {};
    my $ifcrelaggregates = {};
    for my $id (0 .. scalar @{$self->{data}} -1)
    {
        next unless defined $self->{data}->[$id];
        if ($self->{data}->[$id]->is_a('ifcrelassociatesmaterial'))
        {
            my $data = $self->{data}->[$id]->{data};
            push @{$ifcrelassociatesmaterial->{$data->[5]}},
                [$data->[1], $data->[4], $data->[5]];
            splice @{$self->{data}}, $id, 1;
            $id--;
        }
    }
    for my $id (0 .. scalar @{$self->{data}} -1)
    {
        next unless defined $self->{data}->[$id];
        if ($self->{data}->[$id]->is_a('ifcrelcontainedinspatialstructure'))
        {
            my $data = $self->{data}->[$id]->{data};
            push @{$ifcrelcontainedinspatialstructure->{$data->[5]}},
                [$data->[1], $data->[4], $data->[5]];
            splice @{$self->{data}}, $id, 1;
            $id--;
        }
    }
    for my $id (0 .. scalar @{$self->{data}} -1)
    {
        next unless defined $self->{data}->[$id];
        if ($self->{data}->[$id]->is_a('ifcreldefinesbytype'))
        {
            my $data = $self->{data}->[$id]->{data};
            push @{$ifcreldefinesbytype->{$data->[5]}},
                [$data->[1], $data->[4], $data->[5]];
            splice @{$self->{data}}, $id, 1;
            $id--;
        }
    }
    for my $id (0 .. scalar @{$self->{data}} -1)
    {
        next unless defined $self->{data}->[$id];
        if ($self->{data}->[$id]->is_a('ifcrelaggregates'))
        {
            my $data = $self->{data}->[$id]->{data};
            push @{$ifcrelaggregates->{$data->[4]}},
                [$data->[1], $data->[4], $data->[5]];
            splice @{$self->{data}}, $id, 1;
            $id--;
        }
    }
    for my $line (keys %{$ifcrelassociatesmaterial})
    {
        my $item = $ifcrelassociatesmaterial->{$line};
        $self->IFCRELASSOCIATESMATERIAL (guid(), $item->[0]->[0], undef, undef,
            [map {@{$_->[1]}} @{$item}], $item->[0]->[2]);
    }
    for my $line (keys %{$ifcrelcontainedinspatialstructure})
    {
        my $item = $ifcrelcontainedinspatialstructure->{$line};
        $self->IFCRELCONTAINEDINSPATIALSTRUCTURE (guid(), $item->[0]->[0], undef, undef,
            [map {@{$_->[1]}} @{$item}], $item->[0]->[2]);
    }
    for my $line (keys %{$ifcreldefinesbytype})
    {
        my $item = $ifcreldefinesbytype->{$line};
        $self->IFCRELDEFINESBYTYPE (guid(), $item->[0]->[0], undef, undef,
            [map {@{$_->[1]}} @{$item}], $item->[0]->[2]);
    }
    for my $line (keys %{$ifcrelaggregates})
    {
        my $item = $ifcrelaggregates->{$line};
        $self->IFCRELAGGREGATES (guid(), $item->[0]->[0], undef, undef,
            $item->[0]->[1], [map {@{$_->[2]}} @{$item}]);
    }
    $self->{_cache_id} = {};
}

=item Refcount

Count number of references to each entity, increments _refcount for each entity found:

  $ifc->Refcount;

=cut

sub Refcount
{
    my $self = shift;
    for my $id (0 .. scalar @{$self->{data}})
    {
        next unless defined $self->{data}->[$id];
        $self->{data}->[$id]->{_refcount} = 0 unless defined $self->{data}->[$id]->{_refcount};
        File::IFC::Entity->refcount (@{$self->{data}->[$id]->{data}});
    }
    return;
}

=item Get_Id by_id

  $id = $ifc->Get_Id ($entity);

Returns an integer id prefixed with a hash, e.g: #1234

  $entity = $ifc->by_id ($id);

Returns the entity given an integer id

=back

=cut

sub Get_Id
{
    my $self = shift;
    my $entity = shift;
    return $self->{_cache_id}->{$entity} if defined $self->{_cache_id}->{$entity};
    for my $id (1 .. scalar @{$self->{data}})
    {
        return "#$id" if (defined $self->by_id($id) and $self->by_id($id) == $entity);
    }
    return;
}

sub by_id
{
    my $self = shift;
    my $id = shift;
    return $self->{data}->[$id -1];
}

=head1 FUNCTIONS

=over

=item guid

Create a random (probably) unique UID:

  $string = guid();

..or pass a seed and get a reproducible ID:

  $string = guid('Sheffield');

=cut

sub guid
{
    my $seed = shift || time . rand;
    my $guid = Digest::MD5::md5_base64 ($seed);
    $guid =~ s/^./0/x;
    $guid =~ s/[\/+]/0/gx;
    return "$guid";
}

sub real
{
    my $number = shift;
    if ($number =~ /^-?[0-9]+$/)
    {
        return "$number."
    }
    elsif ($number =~ /^(-?[0-9]+\.)0+$/)
    {
        return $1;
    }
    return $number;
}

=back

=cut

1;
