package File::IFC::Header;

use strict;
use warnings;
use File::IFC::Entity;

use base 'File::IFC::Entity';

=head1 Header

File::IFC::Header - a single entity in the HEADER description of an IFC file

=head1 SYNOPSIS

Class representing a HEADER entity, i.e a single line in an IFC file

=head1 DESCRIPTION

=over

This sub-classes the File::IFC::Entity class, the main difference is that HEADER
items don't have unique numeric ids, so the Parse and Dump() methods are different.

=item Parse Dump

Parse a string of an entire line and update the header:

  $header->Parse ($line);

Get a string of the entire line for writing to the IFC file

  $line = $header->Dump;

=cut

our $NAME = '[_[:alnum:]]+';
our $SPACE = '[[:space:]]*';

sub Parse
{
    my $self = shift;
    my $text = shift;
    $text =~ s/\/\*[^*]*\*\///xg;
    my ($key, $data) = $text =~ /^$SPACE($NAME)$SPACE\($SPACE(.*)$SPACE\);/x;
    $self->{ifc}->{header}->{$key} = $self;
    $self->{class} = $key;
    $self->{data} = File::IFC::Entity::dissemble ($data, $self->{ifc});
    return;
}

sub Dump
{
    my $self = shift;
    return $self->is_a . File::IFC::Entity::assemble (@{$self->{data}}) .';';
}

1;
