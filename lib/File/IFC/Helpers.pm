package File::IFC::Helpers;
package File::IFC;

use strict;
use warnings;
use Carp;
use 5.010;
use lib ('lib', '../lib');
use File::IFC qw /real/;
use Time::Piece;
use Math::Trig;
use File::DXF;
use File::DXF::Util qw /read_DXF/;
use File::DXF::Math qw /is_between_2d normalise_2d distance_2d angle_2d subtract_2d rotate_3d/;

=head1 Header

File::IFC::Helpers - Some methods to simplify IFC access

=head1 SYNOPSIS

Enable extra methods for writing IFC files:

  use File::IFC;
  use File::IFC::Helpers;
  my $ifc = new File::IFC;

=head1 DESCRIPTION

This extends the File::IFC class with some optional useful methods for
performing common IFC tasks.

=head1 METHODS

=over

=item Headers

Add a set of valid IFC4 headers, using the current date and whatever is in $ENV{USER}:

  $ifc->Headers;

=cut

sub Headers
{
    my $ifc = shift;
    my $schema = shift || 'IFC4';
    my $exe = $0;
    $exe =~ s/.*[\/\\]//;
    my $user = $ENV{USER} || '';
    my $software = 'File::IFC-'.$File::IFC::VERSION;
    my $time = localtime;

    $ifc->FILE_DESCRIPTION (['ViewDefinition[DesignTransferView]'],'2;1');
    $ifc->FILE_NAME ($exe, $time->datetime, [$user], [''], $software, '', '');
    $ifc->FILE_SCHEMA ([$schema]);
    return;
}

=item OwnerHistory GeoContext Project

Create an IFCOWNERHISTORY entity and associated IFCPERSON, IFCORGANIZATION
etc.. entities, using default values.  Many further IFC entities will need to
reference this $ownerhistory:

  my $ownerhistory = $ifc->OwnerHistory;

Create an IFCGEOMETRICPRESENTATIONCONTEXT entity, coordinates are optional:

  my ($body_context, $axis_context) = $ifc->GeoContext ([0.0,0.0,0.0]);

Create an IFCPROJECT entity, units are radians, kg, kN, m (if you need other units, then you need to write another method):

  my $project = $ifc->Project (geocontexts => $geocontexts,
                             ownerhistory => $ownerhistory,
                                    title => 'My Big Project');

=cut

sub OwnerHistory
{
    my $ifc = shift;

    my $person       = $ifc->IFCPERSON (undef, $ENV{USER}, undef, undef, undef, undef, undef, undef);
    my $organisation = $ifc->IFCORGANIZATION (undef, 'None', undef, undef, undef);
    my $person_org   = $ifc->IFCPERSONANDORGANIZATION ($person, $organisation, undef);
    my $application  = $ifc->IFCAPPLICATION ($organisation, 'STRING:'.$File::IFC::VERSION, 'File::IFC', 'Unknown');
    my $time = time();
    return             $ifc->IFCOWNERHISTORY ($person_org, $application, undef, '.ADDED.', $time, undef, undef, $time);
}

sub GeoContext
{
    my $ifc = shift;
    my $coor_3d       = shift || [0.0,0.0,0.0];
    my $origin_3d     = $ifc->IFCCARTESIANPOINT ($coor_3d);
    my $rotationaxis  = $ifc->IFCDIRECTION(['0.','0.','1.']);
    my $rotationdirection = $ifc->IFCDIRECTION(['1.','0.','0.']);
    my $axis_3d       = $ifc->IFCAXIS2PLACEMENT3D ($origin_3d, $rotationaxis, $rotationdirection);
    my $model_context = $ifc->IFCGEOMETRICREPRESENTATIONCONTEXT (undef, 'Model', 3, undef, $axis_3d, undef);
    my $plan_context  = $ifc->IFCGEOMETRICREPRESENTATIONCONTEXT (undef, 'Plan', 2, undef, $axis_3d, undef);
    my $body_context  = $ifc->IFCGEOMETRICREPRESENTATIONSUBCONTEXT ('Body', 'Model','*','*','*','*',$model_context,undef,'.MODEL_VIEW.',undef);
    my $axis_context  = $ifc->IFCGEOMETRICREPRESENTATIONSUBCONTEXT ('Axis', 'Model','*','*','*','*',$model_context,undef,'.GRAPH_VIEW.',undef);
    my $footprint_context = $ifc->IFCGEOMETRICREPRESENTATIONSUBCONTEXT ('FootPrint', 'Plan','*','*','*','*',$plan_context,undef,'.PLAN_VIEW.',undef);
    my $annotation_context = $ifc->IFCGEOMETRICREPRESENTATIONSUBCONTEXT ('Annotation', 'Plan','*','*','*','*',$plan_context,undef,'.PLAN_VIEW.',undef);
    return ($body_context, $axis_context, $footprint_context, $annotation_context);
}

sub Project
{
    my ($ifc, @temp) = @_;
    my $h = {@temp};
    my $unit_angle  = $ifc->IFCSIUNIT ('*', '.PLANEANGLEUNIT.', undef, '.RADIAN.');
    my $unit_mass   = $ifc->IFCSIUNIT ('*', '.MASSUNIT.', '.KILO.', '.GRAM.');
    my $unit_force  = $ifc->IFCSIUNIT ('*', '.FORCEUNIT.', '.KILO.', '.NEWTON.');
    my $unit_length = $ifc->IFCSIUNIT ('*', '.LENGTHUNIT.', undef, '.METRE.');
    my $unit_assign = $ifc->IFCUNITASSIGNMENT ([$unit_angle, $unit_mass, $unit_force, $unit_length]);
    return            $ifc->IFCPROJECT (guid, $h->{ownerhistory}, $h->{title}, '', undef, '', '', $h->{geocontexts}, $unit_assign);
}

=item StyleRGB

Create a surface style given a name and red/green/blue
colour definition:

  my $presentation = $ifc->StyleRGB ('Pinkish', 1.0, 0.5, 0.5);

=back

=cut

sub StyleRGB
{
    my ($ifc, $name, @RGB) = @_;

    @RGB = map {$_ eq 0 ? '0.' : $_} @RGB;
    @RGB = map {$_ eq 1 ? '1.' : $_} @RGB;

    my $colour = $ifc->IFCCOLOURRGB (undef,@RGB);
    my $style  = $ifc->IFCSURFACESTYLESHADING ($colour, '0.0');
    return       $ifc->IFCSURFACESTYLE ("STRING:$name", '.BOTH.', [$style]);
}

=head2 Methods that generate and return IFCSHAPEREPRESENTATION entities

=over

=item SweptSolid

Extrude a 2d profile vertically upwards:

  my $shape = $ifc->SweptSolid (body_context => $body_context,
                                footprint => [[0.0,0.0],[1.0,0.0],[1.0,1.0],[0.0,1.0]],
                                height => 2.7,
                                presentation => $presentation);

Use four points for a quadrilateral etc...

=cut

sub SweptSolid
{
    my ($ifc, @temp) = @_;
    my $h = {@temp};

    my $solid        = $ifc->ExtrudedAreaSolid(%{$h});
                       $ifc->IFCSTYLEDITEM ($solid, [$h->{presentation}], undef) if defined $h->{presentation};
    return             $ifc->IFCSHAPEREPRESENTATION ($h->{body_context}, 'Body', 'SweptSolid', [$solid]);
}

sub ExtrudedAreaSolid
{
    my ($ifc, @temp) = @_;
    my $h = {@temp};

    my @path    = map {$ifc->IFCCARTESIANPOINT ([$_->[0], $_->[1]])} @{$h->{footprint}}, $h->{footprint}->[0];
    my $polyline     = $ifc->IFCPOLYLINE ([@path]);
    my $profile      = $ifc->IFCARBITRARYCLOSEDPROFILEDEF ('.AREA.', undef, $polyline);
    my $upwards      = $ifc->IFCDIRECTION (['0.','0.','1.']);
    return             $ifc->IFCEXTRUDEDAREASOLID ($profile, $h->{axis}, $upwards, real($h->{height}));
}

sub SweptSurface
{
    my ($ifc, @temp) = @_;
    my $h = {@temp};

    my @path    = map {$ifc->IFCCARTESIANPOINT ($_)} @{$h->{footprint}};
    my $centreline   = $ifc->IFCPOLYLINE ([@path]);
    my $curve2d      = $ifc->IFCARBITRARYOPENPROFILEDEF('.CURVE.',undef,$centreline);
    my $upwards      = $ifc->IFCDIRECTION(['0.','0.','1.']);
    return             $ifc->IFCSURFACEOFLINEAREXTRUSION($curve2d, $h->{axis}, $upwards, real($h->{height}));
}

sub Clipping
{
    my ($ifc, @temp) = @_;
    my $h = {@temp};
    $h->{agreementflag} = '.T.' unless $h->{agreementflag};

    my $location     = $ifc->IFCCARTESIANPOINT($h->{location});
    my $axis         = $ifc->IFCDIRECTION($h->{axis});
    my $refdirection = $ifc->IFCDIRECTION($h->{refdirection});
    my $position     = $ifc->IFCAXIS2PLACEMENT3D($location, $axis, $refdirection);
    my $basesurface  = $ifc->IFCPLANE($position);
    return $ifc->IFCHALFSPACESOLID($basesurface, $h->{agreementflag});
}

=item Curve2D Profile2D Directrix2D

Create a 2d path consisting of straight segments:

  my $curve2d = $ifc->Curve2D (axis_context => $axis_context, centreline => [[1.0,0.0],[5.0,3.0]]);

Two points indicates a single line.

Create a closed 2D profile for extruding:

  my $ifcprofile = $ifc->Profile2D (profile => [[1.0,0.0],[1.0,1.0],[0.0,1.0]]);

Create a 2D directrix for an extrusion, can be open or closed:

  my $ifcpolyline = $ifc->Directrix2D (closed => 1, profile => [[1.0,0.0],[1.0,1.0],[0.0,1.0]]);

=cut

sub Curve2D
{
    my ($ifc, @temp) = @_;
    my $h = {@temp};

    my @path    = map {$ifc->IFCCARTESIANPOINT ($_)} @{$h->{centreline}};
    my $centreline   = $ifc->IFCPOLYLINE ([@path]);
    return             $ifc->IFCSHAPEREPRESENTATION ($h->{axis_context}, 'Axis', 'Curve2D', [$centreline]);
}

sub Curve3D
{
    my ($ifc, @temp) = @_;
    my $h = {@temp};

    my $pointlist = $ifc->IFCCARTESIANPOINTLIST3D($h->{centreline});
    my $centreline = $ifc->IFCINDEXEDPOLYCURVE($pointlist, undef, '.F.');
    return             $ifc->IFCSHAPEREPRESENTATION ($h->{axis_context}, 'Axis', 'Curve3D', [$centreline]);
}

sub Profile2D
{
    my ($ifc, @temp) = @_;
    my $h = {@temp};

    $h->{profile} = _remove_colinear($h->{profile});
    # note a closed polyline has to start and end with the same point
    push @{$h->{profile}}, $h->{profile}->[0];
    my @profile_new = map {[$_->[0], $_->[1]]} (@{$h->{profile}});

    my @path    = map {$ifc->IFCCARTESIANPOINT ($_)} (@profile_new);
    my $profile      = $ifc->IFCPOLYLINE ([@path]);
    return             $ifc->IFCARBITRARYCLOSEDPROFILEDEF ('.AREA.', undef, $profile);
}

sub Directrix2D
{
    my ($ifc, @temp) = @_;
    my $h = {@temp};

    $h->{path} = _remove_colinear($h->{path}, $h->{closed});
    push @{$h->{path}}, $h->{path}->[0] if $h->{closed};
    my @path    = map {$ifc->IFCCARTESIANPOINT ($_)} (@{$h->{path}});
    return             $ifc->IFCPOLYLINE ([@path]);
}

sub _remove_colinear
{
    my ($nodes_orig, $closed) = @_;
    $closed = 0 unless defined $closed;
    my $nodes = [@{$nodes_orig}];
    return $nodes if scalar(@{$nodes}) < 3;

    my @to_delete;
    my $start_id = 1;
    $start_id = -1 if $closed;
    for ($start_id .. scalar(@{$nodes}) - 2)
    {
        if (is_between_2d($nodes->[$_], $nodes->[$_-1], $nodes->[$_+1]))
        {
            push (@to_delete, $_) if $_ > -1;
            push (@to_delete, $_ + scalar(@{$nodes})) if $_ == -1;
        }
    }
    for (sort {$b <=> $a} @to_delete)
    {
        splice @{$nodes}, $_, 1;
    }
    return $nodes;
}

=item Extrusion

  my $ifcproduct = $ifc->Extrusion (body_context => $body_context, profile => $ifcprofile, directrix => $ifcpolyline);

=cut

sub Extrusion
{
    my ($ifc, @temp) = @_;
    my $h = {@temp};

    # a plane for drawing the extrusion
    my $origin = $ifc->IFCCARTESIANPOINT (['0.0','0.0','0.0']);
    my $position = $ifc->IFCAXIS2PLACEMENT3D ($origin, undef, undef);
    my $surface = $ifc->IFCPLANE ($position);

    my $solid = $ifc->IFCSURFACECURVESWEPTAREASOLID ($h->{profile}, $position, $h->{directrix},
                                                     '0.', '1.', $surface);
    $ifc->IFCSTYLEDITEM ($solid, [$h->{presentation}], undef) if defined $h->{presentation};
    my $shape = $ifc->IFCSHAPEREPRESENTATION ($h->{body_context}, 'Body', 'AdvancedSweptSolid', [$solid]);
    return $ifc->IFCPRODUCTDEFINITIONSHAPE ('My Extrusion', undef, [$shape]);
}

=item Dxf2Brep

Create a brep solid from a POLYLINE triangular mesh DXF file, note there is no
checking that this is a valid closed manifold:

  my $brep = $ifc->Dxf2Brep (body_context => $body_context,
                             path_dxf => 't/data/sash.dxf',
                             presentation => $present);
=cut

sub Dxf2Brep
{
    my ($ifc, @temp) = @_;
    my $h = {@temp};

    my $data = read_DXF ($h->{path_dxf}, 'CP1252');
    my $dxf = File::DXF->new;
    $dxf->Process ($data);

    my ($mesh) = $dxf->{ENTITIES}->Extract_Meshes;
    my $nodes = [map {[split / /, $_]} @{$mesh->[0]}];
    my $triangles = [map {[split / /, $_]} @{$mesh->[1]}];

    return $ifc->Brep (%{$h}, nodes => $nodes, triangles => $triangles);
}

=item SurfaceModel Brep

Create a surface model, $nodes is a list of 3d coordinates, triangles is a list
of 3 or 4 sided faces referencing the coordinates indexed starting at zero:

  my $surface = $ifc->SurfaceModel (body_context => $body_context,
                                    nodes => $nodes,
                                    triangles => $triangles,
                                    presentation => $presentation);

Similarly, create a BREP faceted solid model, note that there is no checking
that the set of faces forms a required closed manifold:

  my $brep = $ifc->SurfaceModel (body_context => $body_context,
                                 nodes => $nodes,
                                 triangles => $triangles,
                                 presentation => $presentation);

=back

=cut

sub SurfaceModel
{
    my ($ifc, @temp) = @_;
    my $h = {@temp};
    my $faces   = $ifc->Faces (%{$h});
    my $faceset = $ifc->IFCOPENSHELL ($faces);
    #my $faceset = $ifc->IFCCONNECTEDFACESET ($faces);
    my $surface = $ifc->IFCFACEBASEDSURFACEMODEL ([$faceset]);
                  $ifc->IFCSTYLEDITEM ($surface, [$h->{presentation}], undef) if defined $h->{presentation};
    return        $ifc->IFCSHAPEREPRESENTATION ($h->{body_context}, 'Facetation', 'SurfaceModel', [$surface]);
}

sub Brep
{
    my ($ifc, @temp) = @_;
    my $h = {@temp};
    my $faces   = $ifc->Faces (%{$h});
    my $faceset = $ifc->IFCCLOSEDSHELL ($faces);
    my $brep    = $ifc->IFCFACETEDBREP ($faceset);
                  $ifc->IFCSTYLEDITEM ($brep, [$h->{presentation}], undef) if defined $h->{presentation};
    return        $ifc->IFCSHAPEREPRESENTATION ($h->{body_context}, 'Body', 'Brep', [$brep]);
}

sub Faces
{
    my ($ifc, @temp) = @_;
    my $h = {@temp};

    my $points;
    for my $node (@{$h->{nodes}})
    {
        my $point = $ifc->IFCCARTESIANPOINT ($node);
        push @{$points}, $point;
    }

    my $faces;
    for my $triangle (@{$h->{triangles}})
    {
        my $polyloop  = $ifc->IFCPOLYLOOP ([map {$points->[$_]} @{$triangle}]);
        my $bound     = $ifc->IFCFACEOUTERBOUND ($polyloop, '.T.');
        push @{$faces}, $ifc->IFCFACE ([$bound]);
    }
    return $faces;
}

sub ShiftPlacement
{
    my $ifc = shift;
    my $refplacement = shift;
    my $vector = shift || ['0.','0.','0.'];
    my $rotate_axis = shift || undef;
    my $rotate_vector = shift || undef;
    my $point        = $ifc->IFCCARTESIANPOINT ($vector);
    my ($dir_axis, $dir_vector);
    $dir_axis        = $ifc->IFCDIRECTION ($rotate_axis) if defined $rotate_axis;
    $dir_vector      = $ifc->IFCDIRECTION ($rotate_vector) if defined $rotate_vector;
    my $axis         = $ifc->IFCAXIS2PLACEMENT3D ($point, $dir_axis, $dir_vector);
    return             $ifc->IFCLOCALPLACEMENT ($refplacement, $axis);
}

=head2 Methods that generate and return building components

=over

=item Slab Space

Create an IFCSLAB entity, relevant precursor entities, styled and assigned to a spatial
structure (presentation is optional):

  $slab = $ifc->Slab (body_context => $body_context,
                    ownerhistory => $ownerhistory,
                          storey => $storey,
                       footprint => [[0.0,0.0],[1.0,0.0],[1.0,1.0],[0.0,1.0]],
                          height => 0.3,
                          soffit => 2.7,
                    presentation => $presentation);

Similarly, create an IFCSPACE entity:

  $space = $ifc->Space (body_context => $body_context,
                      ownerhistory => $ownerhistory,
                            storey => $storey,
                         footprint => [[0.0,0.0],[1.0,0.0],[1.0,1.0],[0.0,1.0]],
                            height => 2.7,
                             floor => 0.0,
                             label => 'Room 12',
                              type => 'Bedroom',
                      presentation => $presentation);

=cut

sub Slab
{
    my ($ifc, @temp) = @_;
    my $h = {@temp};

    my $start        = [@{$h->{footprint}->[0]}];
    my $sweptsolid   = $ifc->SweptSolid (%{$h}, footprint => [map {subtract_2d ($_, $start)} @{$h->{footprint}}]);
    my $product      = $ifc->IFCPRODUCTDEFINITIONSHAPE (undef, undef, [$sweptsolid]);
    my $placement    = $ifc->ShiftPlacement ($h->{storey}->{data}->[5], [@{$start},$h->{soffit}]);
    my $slab         = $ifc->IFCSLAB (guid, $h->{ownerhistory}, 'My Slab', '', 'Slab', $placement, $product, undef, '.FLOOR.');
                       $ifc->IFCRELCONTAINEDINSPATIALSTRUCTURE (guid, $h->{ownerhistory}, '', '', [$slab], $h->{storey});
    return $slab;
}

sub Space
{
    my ($ifc, @temp) = @_;
    my $h = {@temp};

    $h->{presentation}->{data}->[2]->[0]->{data}->[1] = 0.9 if defined $h->{presentation};
    my $start        = [@{$h->{footprint}->[0]}];
    my $sweptsolid   = $ifc->SweptSolid (%{$h}, footprint => [map {subtract_2d ($_, $start)} @{$h->{footprint}}]);
    my $product      = $ifc->IFCPRODUCTDEFINITIONSHAPE (undef, undef, [$sweptsolid]);
    my $placement    = $ifc->ShiftPlacement ($h->{storey}->{data}->[5], [@{$start},$h->{floor}]);
    my $space        = $ifc->IFCSPACE (guid($h->{guid}.$h->{level}.$h->{id}), $h->{ownerhistory}, $h->{label}, '', undef, $placement, $product, join (' ', $h->{label}, $h->{level}, $h->{id}), '.ELEMENT.', $h->{type}, undef);
                       $ifc->IFCRELAGGREGATES (guid, $h->{ownerhistory},undef,undef,$h->{storey},[$space]);
    return $space;
}

sub Joist
{
    my ($ifc, @temp) = @_;
    my $h = {@temp};

    my $start        = [@{$h->{footprint}->[0]}];
    my $sweptsolid   = $ifc->SweptSolid (%{$h}, footprint => [map {subtract_2d ($_, $start)} @{$h->{footprint}}]);
    my $product      = $ifc->IFCPRODUCTDEFINITIONSHAPE(undef, undef, [$sweptsolid]);
    my $placement    = $ifc->ShiftPlacement($h->{storey}->{data}->[5], [@{$start},$h->{soffit}]);
    my $beam         = $ifc->IFCBEAM (guid, $h->{ownerhistory}, 'My Beam', '', 'Beam', $placement, $product, undef, undef);
                       $ifc->IFCRELCONTAINEDINSPATIALSTRUCTURE (guid, $h->{ownerhistory}, '', '', [$beam], $h->{storey});
    return $beam;
}

=item Wall

IFCWALL entities are defined in a similar way:

  $wall = $ifc->Wall (body_context => $body_context, axis_context => $axis_context,
                    ownerhistory => $ownerhistory,
                          storey => $storey,
                       footprint => [[0.0,0.0],[5.0,0.0],[5.0,0.4],[0.0,0.4]],
                          height => 3.0,
                            axis => $axis,
                      centreline => [[0.0,0.2],[5.0,0.2]],
                            name => 'Outside',
                            type => $ifcwalltype,
                     layer_usage => $ifcmateriallayersetusage,
                    presentation => undef);

=cut

sub Opening
{
    my ($ifc, @temp) = @_;
    my $h = {@temp};

    my $footprint = [_align ($h->{centreline}, @{$h->{footprint}})];
    my $along = $footprint->[0]->[0];
    my $sweptsolid   = $ifc->SweptSolid (%{$h}, footprint => [map {subtract_2d ($_, [$along, 0.0])} @{$footprint}]);
    my $product      = $ifc->IFCPRODUCTDEFINITIONSHAPE (undef, undef, [$sweptsolid]);
    my $placement    = $ifc->ShiftPlacement ($h->{wall}->{data}->[5], [$along, '0.', $h->{cill}]);
    my $opening      = $ifc->IFCOPENINGELEMENT (guid, $h->{ownerhistory}, 'My Opening', '', 'Opening', $placement, $product, undef, undef);
                       $ifc->IFCRELVOIDSELEMENT (guid, $h->{ownerhistory}, undef, undef, $h->{wall}, $opening);
    return $opening;
}

sub Wall
{
    my ($ifc, @temp) = @_;
    my $h = {@temp};

    my $sweptsolid   = $ifc->SweptSolid (%{$h}, footprint => [_align ($h->{centreline}, @{$h->{footprint}})]);
    my $curve2d      = $ifc->Curve2D (%{$h}, centreline => [['0.','0.'], [distance_2d (@{$h->{centreline}}), 0.0]]);
    my $product      = $ifc->IFCPRODUCTDEFINITIONSHAPE (undef, undef, [$curve2d, $sweptsolid]);
    my $vec = normalise_2d (subtract_2d ($h->{centreline}->[1], $h->{centreline}->[0]));
    my $placement    = $ifc->ShiftPlacement ($h->{storey}->{data}->[5], [@{$h->{centreline}->[0]}, 0.0], ['0.','0.','1.'], [@{$vec}, 0.0]);
    my $wall         = $ifc->IFCWALL (guid, $h->{ownerhistory}, $h->{name}, '', '', $placement, $product, undef, undef);
                       $ifc->IFCRELCONTAINEDINSPATIALSTRUCTURE (guid, $h->{ownerhistory}, '', '', [$wall], $h->{storey});
                       $ifc->IFCRELASSOCIATESMATERIAL (guid, $h->{ownerhistory}, undef, undef,
                                                       [$wall], $h->{layer_usage}) if defined $h->{layer_usage};
                       $ifc->IFCRELDEFINESBYTYPE (guid, undef, undef, undef, [$wall], $h->{type}) if defined $h->{type};
    return $wall;
}

sub _align
{
    my $line = shift;
    my $angle = angle_2d (@{$line});
    my @results;
    for my $coor_2d (@_)
    {
        my $coor_shifted = subtract_2d ($coor_2d, $line->[0]);
        my $coor_3d = rotate_3d (0-$angle, [@{$coor_shifted}, 0.0]);
        push @results, [$coor_3d->[0], $coor_3d->[1]];
    }
    return @results;
}

=item Window Door

Insert a window or door (using the Door() method) in a wall:

  $window = $ifc->Window (body_context => $body_context,
                        ownerhistory => $ownerhistory,
                              storey => $storey,
                                wall => $wall,
                             product => $ifcproduct,
                           footprint => [[1.0,0.0],[1.9,0.0],[1.9,0.4],[0.0,0.4]],
                              height => 1.8,
                               width => 0.9,
                                cill => 0.8,
                             coor_2d => [1.0,0.2],
                           direction => 0.0);

=back

=cut

sub Window
{
    my ($ifc, @temp) = @_;
    my $h = {@temp};

    my $opening      = $ifc->Opening (%{$h});
    my $placement    = $ifc->ShiftPlacement ($h->{storey}->{data}->[5],
                           [@{$h->{coor_2d}}, $h->{cill}], ['0.','0.','1.'], $h->{direction});

    my $origin = $ifc->IFCCARTESIANPOINT([0,0,0]);
    my $transform = $ifc->IFCCARTESIANTRANSFORMATIONOPERATOR3D(undef, undef, $origin, '1.', undef);
    my $mappeditem = $ifc->IFCMAPPEDITEM($h->{product}, $transform);
    my $shaperepresentation = $ifc->IFCSHAPEREPRESENTATION($h->{body_context}, 'Body', 'MappedRepresentation', [$mappeditem]);
    my $product = $ifc->IFCPRODUCTDEFINITIONSHAPE(undef, undef, [$shaperepresentation]);

    my $window       = $ifc->IFCWINDOW (guid, $h->{ownerhistory}, $h->{name}, undef, undef,
                           $placement, $product, undef, real($h->{height}), real($h->{width}), undef, undef, undef);
                       $ifc->IFCRELCONTAINEDINSPATIALSTRUCTURE (guid, $h->{ownerhistory},
                           undef, undef, [$window], $h->{storey});
                       $ifc->IFCRELFILLSELEMENT (guid, $h->{ownerhistory}, undef, undef, $opening, $window);
    return $window;
}

sub Door
{
    my ($ifc, @temp) = @_;
    my $h = {@temp};

    my $opening      = $ifc->Opening (%{$h});
    my $placement    = $ifc->ShiftPlacement ($h->{storey}->{data}->[5],
                           [@{$h->{coor_2d}}, $h->{cill}], ['0.','0.','1.'], $h->{direction});

    my $origin = $ifc->IFCCARTESIANPOINT([0,0,0]);
    my $transform = $ifc->IFCCARTESIANTRANSFORMATIONOPERATOR3D(undef, undef, $origin, '1.', undef);
    my $mappeditem = $ifc->IFCMAPPEDITEM($h->{product}, $transform);
    my $shaperepresentation = $ifc->IFCSHAPEREPRESENTATION($h->{body_context}, 'Body', 'MappedRepresentation', [$mappeditem]);
    my $product = $ifc->IFCPRODUCTDEFINITIONSHAPE(undef, undef, [$shaperepresentation]);

    my $door       = $ifc->IFCDOOR (guid, $h->{ownerhistory}, $h->{name}, undef, undef,
                           $placement, $product, undef, real($h->{height}), real($h->{width}), undef, undef, undef);
                       $ifc->IFCRELCONTAINEDINSPATIALSTRUCTURE (guid, $h->{ownerhistory},
                           undef, undef, [$door], $h->{storey});
                       $ifc->IFCRELFILLSELEMENT (guid, $h->{ownerhistory}, undef, undef, $opening, $door);
    return $door;
}

1;
