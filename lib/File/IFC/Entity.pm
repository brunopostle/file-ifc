package File::IFC::Entity;

use strict;
use warnings;

=head1 NAME

File::IFC::Entity - a single entity in the DATA description of an IFC file

=head1 SYNOPSIS

Class representing a DATA entity, i.e a single line in an IFC file

=head1 DESCRIPTION

=over

=item new

Create a new object, note the $ifc reference to an File::IFC object container:

  $entity = new File::IFC::Entity ('IFCDIRECTION', $ifc);

=cut

our $ENUM = '\.[_A-Z0-9]+\.';
our $NAME = '[_[:alnum:]]+';
our $SPACE = '[[:space:]]*';
our $INTEGER = '[0-9]+';

sub new
{
    my $class = shift;
    my $ifc_class = shift;
    my $ifc = shift;

    my $self = {class => $ifc_class, ifc => $ifc, data => []};

    bless $self, $class;
    return $self;
}

=item Add

Push a list of attributes to the list of attributes:

  $entity->Add ('Stuff Here', 1224, undef, $another_entity);

=cut

sub Add
{
    my ($self, @data) = @_;
    push @{$self->{data}}, @data;
    return;
}

=item Id

Get the unique numeric id (the position in the File::IFC container) prefixed with a hash:

  $id = $entity->Id;

=cut

sub Id
{
    my $self = shift;
    return $self->{ifc}->Get_Id ($self);
}

=item is_a

Get the class name of the entity:

  $name = $entity->is_a;

=cut

sub is_a
{
    my $self = shift;
    my $query = shift;
    if ($query)
    {
        if (lc($query) eq lc($self->{class}))
        {
            return 1;
        }
        else
        {
            return 0
        }
    }
    return $self->{class};
}

=item Parse Dump

Parse a string of an entire line and update the entity:

  $entity->Parse ($line);

Get a string of the entire line for writing to the IFC file:

  $line = $entity->Dump;

=cut

sub Parse
{
    my $self = shift;
    my $text = shift;
    $text =~ s/\/\*[^*]*\*\///xg;
    my ($id, $class, $data) = $text =~ /^#($INTEGER)$SPACE=$SPACE($NAME)$SPACE\($SPACE(.*)$SPACE\);/;
    $self->{ifc}->{data}->[$id -1] = $self unless defined $self->{ifc}->by_id($id);
    $self->{ifc}->by_id($id)->{class} = $class;
    $self->{ifc}->by_id($id)->{data} = dissemble ($data, $self->{ifc});
    $self = $self->{ifc}->by_id($id);
    return;
}

sub Dump
{
    my $self = shift;
    return $self->Id .'='. $self->is_a . assemble (@{$self->{data}}) .';';
}

=item dissemble assemble refcount

Utility function for parsing a string and turning it into a list of attributes,
nesting lists where required:

  $list = dissemble ($text);

Utility function for writing a list of attributes as a string, recurses for
nested lists:

  $text = assemble (@{$self->{data}});

Utility function for counting references to entities, increments _refcount for
each entity found, recurses for nested lists:

  refcount (@{$self->{data});

=back

=cut

sub dissemble
{
    my $text = shift;
    my $ifc = shift;
    my $COMMA = "[[:space:],]+";
    my $QUOTED = "'.*?'";
    my $SIMPLE = "[^',)(]+";
    my $PARAMVALUE = "$NAME\\(.*?\\)";
    my $PARAMQUOTED = "$NAME\\('.*?'\\)";
    my $SUBBRACKETED = "\\((?:$COMMA|$PARAMQUOTED|$PARAMVALUE|$QUOTED|$SIMPLE)*\\)";
    my $BRACKETED = "\\((?:$COMMA|$SUBBRACKETED|$PARAMQUOTED|$PARAMVALUE|$QUOTED|$SIMPLE)*\\)";
    my @tokens = $text =~ /($PARAMQUOTED|$PARAMVALUE|$BRACKETED|$QUOTED|$SIMPLE)/xg;

    my @out;
    for my $token (@tokens)
    {
        $token =~ s/^$COMMA//x;
        $token =~ s/$SPACE$//x;
        if ($token =~ /^\((.*)\)$/x)
        {
            push @out, dissemble ($1, $ifc);
        }
        elsif ($token =~ /#($INTEGER)/)
        {
            my $temp = File::IFC::Entity->new (undef, $ifc);
            $ifc->{data}->[$1 -1] = $temp unless defined $ifc->{data}->[$1 -1];
            push @out, $ifc->{data}->[$1 -1];
        }
        elsif ($token eq '$')
        {
            push @out, undef;
        }
        elsif ($token =~ /^'(.*)'$/x)
        {
            $token = $1;
            $token =~ s/\\X\\27/'/xg;
            push @out, "STRING:$token"; # force string context
        }
        elsif ($token =~ /^$ENUM$/x)
        {
            push @out, "$token";
        }
        elsif ($token =~ /\./x)
        {
            push @out, $token;
        }
        elsif ($token eq '')
        {
        }
        else
        {
            push @out, "$token";
        }
    }
    return [@out];
}

sub assemble
{
    my @items = @_;
    my $text = '(';
    for my $item (@items)
    {
        if (ref $item  and ref $item eq 'File::IFC::Entity')
        {
            $text .= $item->Id;
        }
        elsif (ref $item and ref $item eq 'ARRAY')
        {
            $text .= assemble (@{$item});
        }
        elsif (ref $item)
        {
        }
        elsif (!defined $item)
        {
            $text .= '$';
        }
        elsif ($item =~ /^$ENUM$/x)
        {
            $text .= $item;
        }
        elsif ($item =~ /^[-0-9.]+[eE][+-]?[0-9]+$/x)
        {
            $item =~ s/e/E/x;              # perl uses 'e' in exponential notation, IFC uses 'E'
            $item =~ s/^([-0-9]+)E/$1.E/x; # IFC requires a decimal point in floats
            $text .= $item;
        }
        elsif ($item =~ /^[-0-9]+\.[0-9]*$/x)
        {
            $item =~ s/0*$//x;
            $text .= $item;
        }
        elsif ($item =~ /^[-0-9]+$/x)
        {
            $text .= $item;
        }
        elsif ($item =~ /^$NAME\(.*?\)$/x)
        {
            $text .= $item;
        }
        elsif ($item eq '*')
        {
            $text .= '*';
        }
        elsif ($item =~ /[^-0-9.]/x)
        {
            my $item2 = $item;
            $item2 =~ s/^STRING://x;
            $item2 =~ s/'/\\X\\27/xg;
            $text .= "'$item2'";
        }
        else
        {
            $text .= "''";
        }

        $text .= ',';
    }
    $text =~ s/,?$/)/x;
    return $text;
}

sub refcount
{
    my @items = @_;
    for my $item (@items)
    {
        next unless (ref $item);
        if (ref $item eq 'File::IFC::Entity')
        {
            $item->{_refcount}++;
        }
        if (ref $item eq 'ARRAY')
        {
            refcount (@{$item});
        }
    }
    return;
}

1;
