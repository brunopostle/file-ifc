#!/usr/bin/perl

use strict;
use warnings;
use Carp;
use lib ('lib', '../lib');
use File::IFC;
use 5.010;

my $ifc = File::IFC->new;

open my $IN, '<', $ARGV[0] or croak "$!";
$ifc->Parse (<$IN>);
close $IN;

$ifc->Renumber;
$ifc->{data} = [sort {$a->is_a cmp $b->is_a} @{$ifc->{data}}];

open my $OUT, '>', $ARGV[1] or croak "$!";
print $OUT $ifc->Text;
close $OUT;

0;

__END__

=head1 NAME

ifc-sort - order IFC entities by name

=head1 SYNOPSIS

  ifc-sort.pl project.ifc project-sorted.ifc

=head1 DESCRIPTION

B<ifc-sort> parses an IFC file and rewrites the entities in alphabetical order
- e.g IFCAPPLICATION at the beginning and IFCWINDOW entities at the end. It
also renumbers them starting at #1.

=head1 LICENSE

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

=head1 SEE ALSO

L<File::IFC>

=head1 AUTHOR

Bruno Postle - June 2020.

=cut
