#!/usr/bin/perl

use strict;
use warnings;
use lib ('lib', '../lib');
use File::IFC ('guid');
use File::IFC::Helpers;
use 5.010;
use Time::Piece;
my $software = 'File::IFC-'.$File::IFC::VERSION;
my $time = localtime;

my $ifc = File::IFC->new;

# some headers
$ifc->Headers('IFC4');

# start the project
my $ownerhistory = undef;
my ($body_context, $axis_context) = $ifc->GeoContext(['0.','0.','0.']);
my $axis = $body_context->{data}->[6]->{data}->[4];

my $project      = $ifc->Project (geocontexts => [$body_context->{data}->[6], $axis_context->{data}->[6]],
                                 ownerhistory => $ownerhistory,
                                        title => 'Example Project');

# define a site and put it in the project
my $placement    = $ifc->IFCLOCALPLACEMENT (undef, $axis);
my $site         = $ifc->IFCSITE (guid, $ownerhistory, 'My Site', '', undef, $placement,
                                  undef, undef, '.ELEMENT.', undef, undef, undef, undef, undef);
                   $ifc->IFCRELAGGREGATES (guid, $ownerhistory, '', '', $project, [$site]);

# define a building and put it in the site
$placement       = $ifc->IFCLOCALPLACEMENT (undef, $axis);
my $building     = $ifc->IFCBUILDING (guid, $ownerhistory, 'My Building', '', undef, $placement,
                                  undef, undef, '.ELEMENT.', undef, undef, undef);
                   $ifc->IFCRELAGGREGATES (guid, $ownerhistory, '', '', $site, [$building]);

# define a storey and put it in the building
my $origin_0     = $ifc->IFCCARTESIANPOINT (['0.', '0.', '0.']);
my $axis_0       = $ifc->IFCAXIS2PLACEMENT3D ($origin_0, undef, undef);
$placement       = $ifc->IFCLOCALPLACEMENT (undef, $axis_0);
my $storey_0     = $ifc->IFCBUILDINGSTOREY (guid, $ownerhistory, 'Ground Floor', '', undef, $placement,
                                  undef, undef, '.ELEMENT.', '0.');
                   $ifc->IFCRELAGGREGATES (guid, $ownerhistory, '', '', $building, [$storey_0]);

# define another storey and put it in the building
my $origin_1     = $ifc->IFCCARTESIANPOINT (['0.', '0.', '3.']);
my $axis_1       = $ifc->IFCAXIS2PLACEMENT3D ($origin_1, undef, undef);
$placement       = $ifc->IFCLOCALPLACEMENT (undef, $axis_1);
my $storey_1     = $ifc->IFCBUILDINGSTOREY (guid, $ownerhistory, 'First Floor', '', undef, $placement,
                                  undef, undef, '.ELEMENT.', '3.');
                   $ifc->IFCRELAGGREGATES (guid, $ownerhistory, '', '', $building, [$storey_1]);

# external wall construction
my $block        = $ifc->IFCMATERIAL ('Concrete', undef, undef);
my $block_layer  = $ifc->IFCMATERIALLAYER ($block, 0.160, '.F.', undef, undef, undef, undef);
my $insul        = $ifc->IFCMATERIAL ('Mineral Wool', undef, undef);
my $insul_layer  = $ifc->IFCMATERIALLAYER ($insul, 0.070, '.F.', undef, undef, undef, undef);
my $brick        = $ifc->IFCMATERIAL ('Brick', undef, undef);
my $brick_layer  = $ifc->IFCMATERIALLAYER ($brick, 0.100, '.F.', undef, undef, undef, undef);
my $wallset      = $ifc->IFCMATERIALLAYERSET ([$block_layer, $insul_layer, $brick_layer], 'My External Wall', undef);
my $external     = $ifc->IFCMATERIALLAYERSETUSAGE ($wallset, '.AXIS2.', '.POSITIVE.', -0.080, undef);

# internal wall construction
$block_layer     = $ifc->IFCMATERIALLAYER ($block, 0.160, undef, undef, undef, undef, undef);
$wallset         = $ifc->IFCMATERIALLAYERSET ([$block_layer],'My Internal Wall', undef);
my $internal     = $ifc->IFCMATERIALLAYERSETUSAGE ($wallset, '.AXIS2.', '.POSITIVE.', -0.080, undef);

# some colour
my $present      = $ifc->StyleRGB ('Pinkish', 0.90, 0.80, 0.70);

my $height       = 2.4;

my $wall_a = $ifc->Wall (ownerhistory => $ownerhistory, body_context => $body_context, axis_context => $axis_context, axis => $axis, storey => $storey_1,
                         footprint => [[1.4313,1.5662],[6.9042,2.5204],[6.4503,2.7763],[1.6997,1.9479]],
                         centreline => [[1.6346,1.8554],[6.5603,2.7142]],
                         height => $height, layer_usage => $external, presentation => $present);


my $wall_b = $ifc->Wall (ownerhistory => $ownerhistory, body_context => $body_context, axis_context => $axis_context, axis => $axis,  storey => $storey_1,
                         footprint => [[6.9042,2.5204],[5.4225,6.3224],[5.2131,5.9509],[6.4503,2.7763]],
                         centreline => [[6.5603,2.7142],[5.2639,6.0410]],
                         height => $height, layer_usage => $external, presentation => $present);

                   $ifc->IFCRELCONNECTSPATHELEMENTS (guid, $ownerhistory, undef, undef, undef,
                                                     $wall_b, $wall_a, [], [], '.ATEND.', '.ATSTART.');

my $wall_c = $ifc->Wall (ownerhistory => $ownerhistory, body_context => $body_context, axis_context => $axis_context, axis => $axis,  storey => $storey_1,
                         footprint => [[5.4225,6.3224],[4.1922,6.1079],[3.9238,5.7261],[5.2131,5.9509]],
                         centreline => [[5.2639,6.0410],[3.9889,5.8187]],
                         height => $height, layer_usage => $external, presentation => $present);

                   $ifc->IFCRELCONNECTSPATHELEMENTS (guid, $ownerhistory, undef, undef, undef,
                                                     $wall_c, $wall_b, [], [], '.ATEND.', '.ATSTART.');

my $wall_d = $ifc->Wall (ownerhistory => $ownerhistory, body_context => $body_context, axis_context => $axis_context, axis => $axis,  storey => $storey_1,
                         footprint => [[4.1922,6.1079],[3.7456,8.6693],[3.4772,8.2875],[3.9238,5.7261]],
                         centreline => [[3.9889,5.8187],[3.5423,8.3800]],
                         height => $height, layer_usage => $external, presentation => $present);

                   $ifc->IFCRELCONNECTSPATHELEMENTS (guid, $ownerhistory, undef, undef, undef,
                                                     $wall_d, $wall_c, [], [], '.ATEND.', '.ATSTART.');

my $wall_e = $ifc->Wall (ownerhistory => $ownerhistory, body_context => $body_context, axis_context => $axis_context, axis => $axis,  storey => $storey_1,
                         footprint => [[3.7456,8.6693],[0.2976,8.0681],[0.6794,7.7996],[3.4772,8.2875]],
                         centreline => [[3.5423,8.3800],[0.5868,7.8647]],
                         height => $height, layer_usage => $external, presentation => $present);

                   $ifc->IFCRELCONNECTSPATHELEMENTS (guid, $ownerhistory, undef, undef, undef,
                                                     $wall_e, $wall_d, [], [], '.ATEND.', '.ATSTART.');

my $wall_f = $ifc->Wall (ownerhistory => $ownerhistory, body_context => $body_context, axis_context => $axis_context, axis => $axis,  storey => $storey_1,
                         footprint => [[0.2976,8.0681],[0.7872,5.2604],[1.1123,5.3171],[0.6794,7.7996]],
                         centreline => [[0.5868,7.8647],[1.0334,5.3034]],
                         height => $height, layer_usage => $external, presentation => $present);

                   $ifc->IFCRELCONNECTSPATHELEMENTS (guid, $ownerhistory, undef, undef, undef,
                                                     $wall_f, $wall_e, [], [], '.ATEND.', '.ATSTART.');

my $wall_g = $ifc->Wall (ownerhistory => $ownerhistory, body_context => $body_context, axis_context => $axis_context, axis => $axis,  storey => $storey_1,
                         footprint => [[0.7872,5.2604],[1.4313,1.5662],[1.6997,1.9479],[1.1123,5.3171]],
                         centreline => [[1.0334,5.3034],[1.6346,1.8554]],
                         height => $height, layer_usage => $external, presentation => $present);

                   $ifc->IFCRELCONNECTSPATHELEMENTS (guid, $ownerhistory, undef, undef, undef,
                                                     $wall_g, $wall_f, [], [], '.ATEND.', '.ATSTART.');

                   $ifc->IFCRELCONNECTSPATHELEMENTS (guid, $ownerhistory, undef, undef, undef,
                                                     $wall_a, $wall_g, [], [], '.ATEND.', '.ATSTART.');

# internal wall
my $wall_h = $ifc->Wall (ownerhistory => $ownerhistory, body_context => $body_context, axis_context => $axis_context, axis => $axis,  storey => $storey_1,
                         footprint => [[1.0472,5.2245],[4.0026,5.7399],[3.9751,5.8975],[1.0197,5.3822]],
                         centreline => [[1.0334,5.3034],[3.9889,5.8187]],
                         height => $height, layer_usage => $internal, presentation => $present);

                   $ifc->IFCRELCONNECTSPATHELEMENTS (guid, $ownerhistory, undef, undef, undef,
                                                     $wall_h, $wall_c, [], [], '.ATEND.', '.ATEND.');

                   $ifc->IFCRELCONNECTSPATHELEMENTS (guid, $ownerhistory, undef, undef, undef,
                                                     $wall_h, $wall_f, [], [], '.ATEND.', '.ATSTART.');

                   $ifc->IFCRELCONNECTSPATHELEMENTS (guid, $ownerhistory, undef, undef, undef,
                                                     $wall_h, $wall_g, [], [], '.ATSTART.', '.ATSTART.');

eval 'use File::DXF';
if ($@)
{
    say STDERR 'File::DXF not found, skipping IFCWINDOW entities';
}
else
{
my $brep_sash = $ifc->Dxf2Brep (body_context => $body_context, path_dxf => 't/data/sash.dxf', presentation => $present);
my $product_sash = $ifc->IFCPRODUCTDEFINITIONSHAPE (undef, undef, [$brep_sash]);

             $ifc->Window (ownerhistory => $ownerhistory, body_context => $body_context, axis => $axis,  storey => $storey_1,
                           wall => $wall_a, product => $product_sash, cill => 0.3, height => 1.82, width => 0.91,
                           coor_2d => [3.2292,2.1334], direction => [0.9851,0.1718,'0.'],
                           footprint => [[3.2894,1.7886],[4.1858,1.9449],[4.0948,2.4671],[3.1983,2.3107]]);

             $ifc->Window (ownerhistory => $ownerhistory, body_context => $body_context, axis => $axis,  storey => $storey_1,
                           wall => $wall_b, product => $product_sash, cill => 0.5, height => 1.82, width => 0.91,
                           coor_2d => [5.7709,4.7399], direction => [-0.3631,0.9317,'0.'],
                           footprint => [[6.0970,4.8670],[5.7666,5.7149],[5.2728,5.5225],[5.6032,4.6746]]);
}

my $slab_0 = $ifc->Slab (ownerhistory => $ownerhistory, body_context => $body_context, axis => $axis,  storey => $storey_1,
                         footprint => [[1.6997,1.9479],[6.4503,2.7763],[5.2131,5.9509],[1.1260,5.2383]],
                         height => 0.3, soffit => -0.3, presentation => $present);

my $space = $ifc->Space (ownerhistory => $ownerhistory, body_context => $body_context, axis => $axis,  storey => $storey_1,
                         guid => 'thisproject', level => 1, id => '102', label => 'kitchen',
                         footprint => [[1.6997,1.9479],[6.4503,2.7763],[5.2131,5.9509],[1.1260,5.2383]],
                         height => 3.0, floor => 0.0, presentation => $present, type => '.INTERNAL.');

# a closed profile
my $profile_def = $ifc->Profile2D (profile => [['0.','0.'], [0.2,'0.'], [0.2,0.2], [0.3,0.2], [0.32,0.32], [0.38,0.38], [0.5,0.4], [0.5,0.5], ['0.',0.6]]);

# the path/directrix for the extrusion
my $directrix = $ifc->Directrix2D (closed => 1, path => [[1.6346,1.8554], [6.5603,2.7142],
                                        [5.2639,6.0410], [3.9889,5.8187], [3.5423,8.3800],
                                        [0.5868,7.8647], [1.0334,5.3034]]);

my $product = $ifc->Extrusion (body_context => $body_context, profile => $profile_def, directrix => $directrix);

# put it in the first storey
$placement = $ifc->ShiftPlacement ($storey_1->{data}->[5], ['0.0','0.0','2.4']);
my $extrusion = $ifc->IFCROOF (guid, $ownerhistory, 'Extrusion', undef, undef, $placement, $product, undef, '.FREEFORM.');
$ifc->IFCRELCONTAINEDINSPATIALSTRUCTURE (guid ,$ownerhistory, '', '', [$extrusion], $storey_1);

my $shape = $ifc->Dxf2Brep (body_context => $body_context, path_dxf => 't/data/column.dxf', presentation => $present);
my $product2 = $ifc->IFCPRODUCTDEFINITIONSHAPE ('My Column', undef, [$shape]);

$placement = $ifc->ShiftPlacement ($storey_1->{data}->[5], ['4.4','4.0','0.0'], [0,0,1], [0.6,0.1,0]);

my $insert = $ifc->IFCCOLUMN (guid, $ownerhistory, 'Column', undef, undef, $placement, $product2, undef, undef);
$ifc->IFCRELCONTAINEDINSPATIALSTRUCTURE (guid, $ownerhistory, '', '', [$insert], $storey_1);

binmode STDOUT, ":crlf";
print STDOUT $ifc->Text;

0;

__END__

=head1 NAME

ifc-example - example IFC generator

=head1 SYNOPSIS

  ifc-example.pl > output.ifc

=head1 DESCRIPTION

B<ifc-example> shows how you can use L<File::IFC> to generate a simple house model in IFC format.
If windows don't appear then you haven't installed the L<File::DXF> module.

=head1 LICENSE

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

=head1 SEE ALSO

L<File::IFC>
L<File::DXF>

=head1 AUTHOR

Bruno Postle - January 2014.

=cut
