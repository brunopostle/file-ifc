#!/usr/bin/perl

use strict;
use warnings;
use Carp;
use lib ('lib', '../lib');
use File::IFC;
use 5.010;

my $ifc = File::IFC->new;

open my $IN, '<', $ARGV[0] or croak "$!";
$ifc->Parse (<$IN>);
close $IN;

$ifc->Refcount;

for my $entity (@{$ifc->{data}})
{
    next unless defined $entity;
    say join ' ', $entity->Id, $entity->is_a, $entity->{_refcount};
}

0;

__END__

=head1 NAME

ifc-refcount - report on IFC entities

=head1 SYNOPSIS

  ifc-refcount.pl project.ifc | less

=head1 DESCRIPTION

B<ifc-refcount> parses an IFC file and lists all the entities indicating how many times they are referenced by other entities.

=head1 LICENSE

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

=head1 SEE ALSO

L<File::IFC>

=head1 AUTHOR

Bruno Postle - January 2014.

=cut
