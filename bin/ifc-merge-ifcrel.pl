#!/usr/bin/perl

use strict;
use warnings;
use Carp;
use lib ('lib', '../lib');
use File::IFC;
use 5.010;

my $ifc = File::IFC->new;

open my $IN, '<', $ARGV[0] or croak "$!";
$ifc->Parse (<$IN>);
close $IN;

$ifc->Merge_Ifcrel;

open my $OUT, '>', $ARGV[1] or croak "$!";
binmode $OUT, ":crlf";
print $OUT $ifc->Text;
close $OUT;

0;

__END__

=head1 NAME

ifc-merge-ifcrel - merge items that associate other items

=head1 SYNOPSIS

  ifc-renumber.pl input.ifc output.ifc

=head1 DESCRIPTION

B<ifc-merge-ifcrel> merges IFCRELAGGREGATES, IFCRELCONTAINEDINSPATIALSTRUCTURE,
IFCRELASSOCIATEMATERIAL and IFCRELCONNECTSPATHELEMENTS entities

=head1 LICENSE

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

=head1 SEE ALSO

L<File::IFC>

=head1 AUTHOR

Bruno Postle - March 2020.

=cut
