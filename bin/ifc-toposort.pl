#!/usr/bin/perl

use strict;
use warnings;
use Carp;
use lib ('lib', '../lib');
use File::IFC;
use 5.010;

my $ifc = File::IFC->new;

open my $IN, '<', $ARGV[0] or croak "$!";
$ifc->Parse (<$IN>);
close $IN;

use Graph::Directed;
use Graph::Traversal::DFS;
use Graph::Traversal::BFS;

my $graph = Graph::Directed->new;
my $count = {};

for my $entity (@{$ifc->{data}})
{
    next unless defined $entity;
    $count->{uc $entity->is_a}++;
    for my $item (@{$entity->{data}})
    {
        if (ref $item and ref $item eq 'File::IFC::Entity')
        {
            $graph->add_edge($entity,$item);
        }
        elsif (ref $item and ref $item eq 'ARRAY')
        {
            for my $item_sub (@{$item})
            {
                if (ref $item_sub and ref $item_sub eq 'File::IFC::Entity')
                {
                    $graph->add_edge($entity,$item_sub);
                }
            }
        }
    }
}

say 'Isolated vertices: '. scalar $graph->isolated_vertices;
say 'Unique vertices: '. scalar $graph->unique_vertices;
say 'Successorful vertices: '. scalar $graph->successorful_vertices;
say 'Predecessorful vertices: '. scalar $graph->predecessorful_vertices;
say 'Successorless vertices: '. scalar $graph->successorless_vertices;
say 'Predecessorless vertices: '. scalar $graph->predecessorless_vertices;
say 'Exterior vertices: '. scalar $graph->exterior_vertices;
say 'Interior vertices: '. scalar $graph->interior_vertices;
say 'Unique edges: '. scalar $graph->unique_edges;
die 'Cyclic graph!' if $graph->is_cyclic;

for my $type (sort keys %{$count})
{
    say $type.': '.$count->{$type};
}

my %opt = (next_successor => \&next_successor);
my $t = Graph::Traversal::DFS->new($graph, %opt);
$ifc->{data} = [$t->dfs];

open my $OUT, '>', $ARGV[1] or croak "$!";
print $OUT $ifc->Text;
close $OUT;

sub next_successor
{
    my ($obj, $hash) = @_;
    my @keys = sort {scalar $graph->successors($hash->{$a}) <=> scalar $graph->successors($hash->{$b})} keys %{$hash};
    return map {$hash->{$_}} @keys;
}

0;

__END__

=head1 NAME

ifc-toposort - Depth first topological sorting of IFC files

=head1 SYNOPSIS

  ifc-toposort.pl project.ifc project-sorted.ifc

=head1 DESCRIPTION

B<ifc-toposort> parses an IFC file and generates a Directed Acyclic Graph of
the relationships between entities. The graph is used to write the output IFC
file such that referenced entities always precede the entities that reference
them. Entities are renumbered during this process.

=head1 LICENSE

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

=head1 SEE ALSO

L<File::IFC>

=head1 AUTHOR

Bruno Postle - June 2020.

=cut
