#!/usr/bin/perl

use strict;
use warnings;
use lib ('lib', '../lib');
use File::IFC ('guid');
use File::IFC::Helpers;
use File::DXF::Util qw /read_DXF/;
use File::DXF;
use 5.010;

my $ifc = File::IFC->new;

# some headers
$ifc->Headers('IFC4');

# start the project
my $ownerhistory = undef;
my ($body_context, $axis_context) = $ifc->GeoContext(['0.','0.','0.']);
my $axis = $body_context->{data}->[6]->{data}->[4];

my $project      = $ifc->Project (geocontexts => [$body_context->{data}->[6], $axis_context->{data}->[6]],
                                ownerhistory => $ownerhistory,
                                       title => 'Example Project');

# define a site and put it in the project
my $placement    = $ifc->IFCLOCALPLACEMENT (undef, $axis);
my $site         = $ifc->IFCSITE (guid, $ownerhistory, 'My Site', '', undef, $placement,
                                  undef, undef, '.ELEMENT.', undef, undef, undef, undef, undef);
                   $ifc->IFCRELAGGREGATES (guid, $ownerhistory, '', '', $project, [$site]);

# define a building and put it in the site
$placement       = $ifc->IFCLOCALPLACEMENT (undef, $axis);
my $building     = $ifc->IFCBUILDING (guid, $ownerhistory, 'My Building', '', undef, $placement,
                                  undef, undef, '.ELEMENT.', undef, undef, undef);
                   $ifc->IFCRELAGGREGATES (guid, $ownerhistory, '', '', $site, [$building]);

# define a storey and put it in the building
my $origin_0     = $ifc->IFCCARTESIANPOINT (['0.','0.','0.']);
my $axis_0       = $ifc->IFCAXIS2PLACEMENT3D ($origin_0, undef, undef);
$placement       = $ifc->IFCLOCALPLACEMENT (undef, $axis_0);
my $storey_0     = $ifc->IFCBUILDINGSTOREY (guid, $ownerhistory, 'Ground Floor', '', undef, $placement,
                                  undef, undef, '.ELEMENT.', '0.');
                   $ifc->IFCRELAGGREGATES (guid, $ownerhistory, '', '', $building, [$storey_0]);

my $data = read_DXF ($ARGV[0], 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);

for my $mesh ($dxf->{ENTITIES}->Extract_Meshes)
{
    my $nodes = [map {[split / /, $_]} @{$mesh->[0]}];
    my $triangles = [map {[split / /, $_]} @{$mesh->[1]}];

    # some colour
    my $colour_dxf = $mesh->[2]->{colour} || 255;
    my $colour = $File::DXF::Util::colourdecimal->[$colour_dxf];
    my @colour = $colour =~ /([0-9]+)/xg;
    @colour = map {$_/255} @colour;
    my $rgb       = $ifc->IFCCOLOURRGB (undef, @colour);
    my $rendering = $ifc->IFCSURFACESTYLERENDERING ($rgb, undef, undef, undef, undef, undef, undef, undef, '.FLAT.');
    my $style     = $ifc->IFCSURFACESTYLE (undef, '.BOTH.', [$rendering]);

    #my $shape     = $ifc->SurfaceModel (body_context => $body_context, nodes => $nodes, triangles => $triangles, presentation => $style);
    my $shape     = $ifc->Brep (body_context => $body_context, nodes => $nodes, triangles => $triangles, presentation => $style);

    my $product   = $ifc->IFCPRODUCTDEFINITIONSHAPE (undef, undef, [$shape]);
    my $proxy     = $ifc->IFCBUILDINGELEMENTPROXY (guid, $ownerhistory, 'Box', undef, undef,
                                                   $placement, $product, undef, undef);
    $ifc->IFCRELCONTAINEDINSPATIALSTRUCTURE (guid, $ownerhistory, '', '', [$proxy], $storey_0);
}

if (defined $ARGV[1])
{
    open my $OUT, '>', $ARGV[1] or die "$!";
    binmode $OUT, ":crlf";
    print $OUT $ifc->Text;
    close $OUT;
}
else
{
    binmode STDOUT, ":crlf";
    print STDOUT $ifc->Text;
}

0;

__END__

=head1 NAME

ifc-dxf2shell - convert DXF polyface meshes into IFC entities

=head1 SYNOPSIS

  ifc-dxf2shell.pl input.ifc output.dxf

=head1 DESCRIPTION

B<ifc-dxf2shell> extracts polyface meshes from a DXF file ENTITIES section, and writes them as IFCBUILDINGELEMENTPROXY entities in a new IFC file.
Will not locate meshes inside BLOCKS, use B<dxfbind> first.
Will not convert rectangular meshes, use B<rectangular2mesh> first.
Will not convert 3DFACE entities, join them with B<3dface2polyline> first.

This script depends on the L<File::DXF> module.

=head1 LICENSE

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

=head1 SEE ALSO

L<File::DXF>
L<File::IFC>

=head1 AUTHOR

Bruno Postle - January 2014.

=cut
