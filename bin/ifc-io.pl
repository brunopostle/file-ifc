#!/usr/bin/perl

use strict;
use warnings;
use Carp;
use lib ('lib', '../lib');
use File::IFC;

my $ifc = File::IFC->new;

open my $IN, '<', $ARGV[0] or croak "$!";
$ifc->Parse (<$IN>);
close $IN;

open my $OUT, '>', $ARGV[1] or croak "$!";
binmode $OUT, ":crlf";
print $OUT $ifc->Text;
close $OUT;

0;

__END__

=head1 NAME

ifc-io - parse and assemble an IFC file

=head1 SYNOPSIS

  ifc-io.pl input.ifc output.ifc

=head1 DESCRIPTION

B<ifc-io> parses an IFC file, and rewrites it.
The output should be functionally identical to the input.
This is only useful for debugging the parser.

=head1 LICENSE

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

=head1 SEE ALSO

L<File::IFC>

=head1 AUTHOR

Bruno Postle - January 2014.

=cut
