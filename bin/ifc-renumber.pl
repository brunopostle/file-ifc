#!/usr/bin/perl

use strict;
use warnings;
use Carp;
use lib ('lib', '../lib');
use File::IFC;
use 5.010;

my $ifc = File::IFC->new;

open my $IN, '<', $ARGV[0] or croak "$!";
$ifc->Parse (<$IN>);
close $IN;

$ifc->Renumber;

open my $OUT, '>', $ARGV[1] or croak "$!";
binmode $OUT, ":crlf";
print $OUT $ifc->Text;
close $OUT;

0;

__END__

=head1 NAME

ifc-renumber - renumber lines in an IFC file

=head1 SYNOPSIS

  ifc-renumber.pl input.ifc output.ifc

=head1 DESCRIPTION

B<ifc-renumber> parses an IFC file, and rewrites all the entity numbers starting at #1.
This is only useful for making IFC files just slightly more human readable.

=head1 LICENSE

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

=head1 SEE ALSO

L<File::IFC>

=head1 AUTHOR

Bruno Postle - January 2014.

=cut
