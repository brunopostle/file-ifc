#!/usr/bin/perl

#Editor vim:syn=perl

use strict;
use warnings;
use Carp;
use Test::More 'no_plan';

use lib ('lib', '../lib');
use_ok ('File::IFC');
use_ok ('File::IFC::Header');

my $ifc = File::IFC->new;

open my $IN, '<', 't/data/test.ifc' or croak "$!";
ok ($ifc->Parse (<$IN>), 'Parse()');
close $IN;

# try and cope with undefined lines
push @{$ifc->{data}}, undef;

my @lines = $ifc->Dump;

is ($lines[0], 'ISO-10303-21;', 'ISO line');
is ($lines[1], 'HEADER;', 'HEADER line');
ok ($lines[2] =~ /^FILE_DESCRIPTION\(.*\);/x, 'header');
ok ($lines[3] =~ /^FILE_NAME\(.*\);/x, 'header');
ok ($lines[4] =~ /^FILE_SCHEMA\(\(.*\)\);/x, 'header');
is ($lines[5], 'ENDSEC;', 'ENDSEC line');
is ($lines[6], 'DATA;', 'DATA line');
ok ($lines[7] =~ /^#1=IFCCARTESIANPOINT\(\(0,0,0\)\);/x, 'data');
ok ($lines[8] =~ /^#2=IFCDIRECTION\(\(0,0,1\)\);/x, 'data');
ok ($lines[9] =~ /^#3=IFCDIRECTION\(\(1,0,0\)\);/x, 'data');
ok ($lines[10] =~ /^#4=IFCAXIS2PLACEMENT3D\(#1,#2,#3\);/x, 'data');
like ($lines[11], '/^#5=IFCFOO\(#6,\*,\$,\$,\.FOO\.\);/x', 'data');
ok ($lines[12] =~ /^#6=IFCBAR\(\*,\*,\*\);/x, 'data');
is ($lines[13], 'ENDSEC;', 'ENDSEC line');
is ($lines[14], 'END-ISO-10303-21;', 'ISO line');

#print $ifc->Text;

my $header = File::IFC::Header->new (undef, $ifc);

$header->Parse ("FILE_DESCRIPTION(('),'),'2;1');");

is ($header->{data}->[0]->[0], 'STRING:),', 'Parse()');
is ($header->{data}->[1], 'STRING:2;1', 'Parse()');

$header->Parse ( "FILE_NAME( /* name */ 'C:\\\\My Work\\\\grasshopper\\\\120510 wall standard case.ifc', /* time_stamp */ '2012-05-10T12:20:19', /* author */ ('Jon'), /* organization */ ('Unknown'), /* preprocessor_version */ 'ssiIFC - Grasshopper Plug-in by Geometry Gym Pty Ltd', /* originating_system */ 'ssiIFC - Grasshopper Plug-in by Geometry Gym Pty Ltd', /* authorization */ 'None');"); 

is ($header->{data}->[6], 'STRING:None', 'Parse()');

#use Data::Dumper; die Dumper $header->{data};

my $entity = File::IFC::Entity->new (undef, $ifc);

my $text = "#48=IFCTRIMMEDCURVE(#54,(IFCPARAMETERVALUE(0.0),#49),(IFCPARAMETERVALUE(0.0780641426618212),#50),.T.,.PARAMETER.);";

$entity->Parse ($text);

ok (ref $entity->{data}->[0] eq 'File::IFC::Entity', 'Parse()');
is ($entity->{data}->[1]->[0], 'IFCPARAMETERVALUE(0.0)', 'Parse()');
ok (ref $entity->{data}->[1]->[1] eq 'File::IFC::Entity', 'Parse()');
is ($entity->{data}->[2]->[0], 'IFCPARAMETERVALUE(0.0780641426618212)', 'Parse()');
ok (ref $entity->{data}->[2]->[1] eq 'File::IFC::Entity', 'Parse()');
is ($entity->{data}->[3], '.T.', 'Parse()');
is ($entity->{data}->[4], '.PARAMETER.', 'Parse()');

is ($entity->Dump, $text, 'roundtrip');

#use Data::Dumper; warn Dumper $entity->{data};

$text = "#48=IFCPROPERTYSINGLEVALUE('Reference',\$,IFCIDENTIFIER('Sash (5):Sash (5)'),\$);";
$entity->Parse ($text);
is ($entity->Dump, $text, 'roundtrip');

$text = '#48=IFCCARTESIANPOINT((0.0E+00,0.0E+00));';
$entity->Parse ($text);
is ($entity->Dump, $text, 'roundtrip');

$text = '#48=IFCVECTOR(#132,2.8E+01);';
$entity->Parse ($text);
is ($entity->Dump, $text, 'roundtrip');

$text = '#48=IFCDIRECTION((1.0E+00,0.0E+00));';
$entity->Parse ($text);
is ($entity->Dump, $text, 'roundtrip');

1;
