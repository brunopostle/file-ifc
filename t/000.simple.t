#!/usr/bin/perl

#Editor vim:syn=perl

use strict;
use warnings;
use Test::More 'no_plan';

use lib ('lib', '../lib');
use File::IFC ('guid');
use_ok ('File::IFC::Entity');

my $ifc = File::IFC->new;

my $point = $ifc->IFCCARTESIANPOINT;
is ($point->Id, '#1', 'Id()');
is ($point->is_a, 'IFCCARTESIANPOINT', 'is_a()');
is ($point->is_a('iFcCARteSianPoiNT'), 1, 'is_a()');
is ($point->is_a('iFcPoiNT'), 0, 'is_a()');
is ($point->Dump, '#1=IFCCARTESIANPOINT();', 'Dump()');

$point->Add ([0.0,0.0,0.0]);
is ($point->Dump, '#1=IFCCARTESIANPOINT((0,0,0));', 'Dump()');

my $direction_x = $ifc->IFCDIRECTION ([0,0,1]);
my $direction_y = $ifc->IFCDIRECTION ([1,0,0]);
my $axis2placement = $ifc->IFCAXIS2PLACEMENT3D ($point, $direction_x, $direction_y);

is ($axis2placement->Dump, '#4=IFCAXIS2PLACEMENT3D(#1,#2,#3);', 'Dump()');

my $foo = $ifc->IFCFOO;
$foo->Add ('STRING:Some text', undef, 12.0);
my $bar = $ifc->IFCBAR ('STRING:Some other text', undef, 12.03, '.ELEMENT.', 1.0E-05);
my $baz = $ifc->IFCBAZ (undef, 'STRING:More text', $foo, $bar);
my $noo = $ifc->IFCNOO ($foo, [0,1,0], undef, [$foo, $bar]);

is ($foo->Dump, '#5=IFCFOO(\'Some text\',$,12);', 'Dump()');
is ($bar->Dump, '#6=IFCBAR(\'Some other text\',$,12.03,.ELEMENT.,1.E-05);', 'Dump()');
is ($baz->Dump, '#7=IFCBAZ($,\'More text\',#5,#6);', 'Dump()');
is ($noo->Dump, '#8=IFCNOO(#5,(0,1,0),$,(#5,#6));', 'Dump()');

my @list = $ifc->Dump;
ok (scalar (@list) > 1, 'File::IFC::Dump()');
my $text = $ifc->Text;
ok ($text =~ /ENDSEC/, 'Text()');

$foo = $ifc->IFCFOO (undef, { height => 'short' }, undef);
is ($foo->Dump, '#9=IFCFOO($,,$);', 'Dump()');

$ifc->DESTROY;

my $des = $ifc->FILE_DESCRIPTION (['IFC 2x platform'],'2;1');
my $nam = $ifc->FILE_NAME ('STRING:Example.dwg', 'STRING:2005-09-02T14:48:42', ['STRING:The User'], ['STRING:The Company'], 'STRING:The name and version of the IFC preprocessor', 'STRING:The name of the originating software system', 'STRING:The name of the authorizing person');
my $sch = $ifc->FILE_SCHEMA (['STRING:IFC2X2_FINAL']);

ok ($ifc->Text =~ /FILE_SCHEMA\(\('IFC2X2_FINAL'\)\);/x, 'Header');

$ifc->FILE_FOO ("STRING:Who's who's who");

my $before = $bar->Dump;
$bar->Parse ($bar->Dump);
my $after = $bar->Dump;
is ($before, $after, 'Parse()');

$before = $noo->Dump;
$noo->Parse ($noo->Dump);
$after = $noo->Dump;
is ($before, $after, 'Parse()');

File::IFC::Entity::dissemble ('#5,(0,1,0),$,(#5,#6)');

is (File::IFC::Entity::assemble (''), "('')", 'assemble()');
is (File::IFC::Entity::assemble ('*'), "(*)", 'assemble()');
is (File::IFC::Entity::assemble ('STRING:2013'), "('2013')", 'assemble()');
is (File::IFC::Entity::assemble (undef), "(\$)", 'assemble()');
is (File::IFC::Entity::assemble (['']), "((''))", 'assemble()');
is (File::IFC::Entity::assemble ([['']]), "((('')))", 'assemble()');
is (File::IFC::Entity::assemble ([['Foo', [undef]]]), "((('Foo',(\$))))", 'assemble()');
is (File::IFC::Entity::assemble ('.FOO.'), "(.FOO.)", 'assemble()');
is (File::IFC::Entity::assemble (12), "(12)", 'assemble()');
is (File::IFC::Entity::assemble (12.1), "(12.1)", 'assemble()');
is (File::IFC::Entity::assemble ('Some text'), "('Some text')", 'assemble()');

$before = $des->Dump;
$des->Parse ($des->Dump);
$after = $des->Dump;
is ($before, $after, 'Parse()');

$before = $nam->Dump;
$nam->Parse ($nam->Dump);
$after = $nam->Dump;
is ($before, $after, 'Parse()');

$before = $sch->Dump;
$sch->Parse ($sch->Dump);
$after = $sch->Dump;
is ($before, $after, 'Parse()');

ok (defined guid(), 'guid');

ok (guid() ne guid(), 'guid unique');
ok (guid('sheffield') eq guid('sheffield'), 'guid repeatable');
ok (guid('leeds') ne guid('sheffield'), 'guid unique');

like (guid(), '/^0[a-zA-Z0-9]{21}$/', 'guid validates') for (0 .. 15);

#print $ifc->Text;

1;
