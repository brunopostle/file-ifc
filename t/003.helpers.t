#!/usr/bin/perl

#Editor vim:syn=perl

use strict;
use warnings;
use Test::More 'no_plan';

use lib ('lib', '../lib');
use_ok ('File::IFC');
use_ok ('File::IFC::Helpers');

my $ifc = File::IFC->new;

is (scalar keys %{$ifc->{header}}, 0, 'no headers');
$ifc->Headers('IFC2X3');
is (scalar keys %{$ifc->{header}}, 3, '3 headers');

is (scalar @{$ifc->{data}}, 0, 'no entities');
my $ownerhistory = $ifc->OwnerHistory;
is (scalar @{$ifc->{data}}, 5, '5 entities');

my $ifcsurfacestyle = $ifc->StyleRGB ('Pinkish', 1.0, 0.5, 0.0);

is ($ifcsurfacestyle->{data}->[0], 'STRING:Pinkish', 'colour name');

my $ifcsurfacestylerendering = $ifcsurfacestyle->{data}->[2]->[0];
my $ifccolourrgb = $ifcsurfacestylerendering->{data}->[0];

is ($ifccolourrgb->{data}->[1], '1.', '1.');
is ($ifccolourrgb->{data}->[2], '0.5', '0.5');
is ($ifccolourrgb->{data}->[3], '0.', '0.');

is (File::IFC::real(0.5), 0.5, 'float preserved');
is (File::IFC::real(1.22008981252869e+18), 1.22008981252869e+18, 'float preserved');
is (File::IFC::real('0.'), '0.', 'float enforced');
is (File::IFC::real(0.0), '0.', 'float enforced');
is (File::IFC::real(-4.0), '-4.', 'float enforced');
is (File::IFC::real('0.0'), '0.', 'float enforced');
is (File::IFC::real('-4.0'), '-4.', 'float enforced');
is (File::IFC::real(-4), '-4.', 'float created');
is (File::IFC::real(0), '0.', 'float created');
is (File::IFC::real('-4'), '-4.', 'float created');
is (File::IFC::real('0'), '0.', 'float created');

my $ifccartesianpoint = $ifc->IFCCARTESIANPOINT ([0,'2.0',4.5]);
my $coor = $ifccartesianpoint->{data}->[0];

is ($coor->[0], '0.', 'float created');
is ($coor->[1], '2.', 'float enforced');
is ($coor->[2], '4.5', 'float preserved');

1;
