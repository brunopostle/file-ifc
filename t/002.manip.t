#!/usr/bin/perl

#Editor vim:syn=perl

use strict;
use warnings;
use Carp;
use 5.010;
use Test::More 'no_plan';

use lib ('lib', '../lib');
use_ok ('File::IFC');

my $ifc = File::IFC->new;

open my $IN, '<', 't/data/Project1-io.ifc' or croak "$!";
$ifc->Parse (<$IN>);
close $IN;

is (scalar @{$ifc->{data}}, 2037, 'last entry 2037');

$ifc->Renumber;
is (scalar @{$ifc->{data}}, 1204, 'last entry 1204 after Renumber()');

$ifc = File::IFC->new;

open $IN, '<', 't/data/Project1-io.ifc' or croak "$!";
$ifc->Parse (<$IN>);
close $IN;

$ifc->Refcount;

is ($ifc->{data}->[51]->Id, '#52', 'Id()');
is ($ifc->by_id(52)->Id, '#52', 'by_id()');
is ($ifc->{data}->[51]->{_refcount}, '153', 'Refcount()');

1;
